package vmorales;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import static javax.media.opengl.GL.GL_PROJECTION;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;



/**
 * FigurasGluyGlut.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class FigurasGluyGlut implements GLEventListener {

    static float ang=0;
    static float ang2=0;
    public static void main(String[] args) {
        Frame frame = new Frame("Simple JOGL Application");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new FigurasGluyGlut());
        frame.add(canvas);
        frame.setSize(800,600);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {
         
            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLU glu = new GLU ();
        GLUT glut = new GLUT ();
           
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        GLUquadric quad;
        quad = glu.gluNewQuadric ();
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
        
        //dibuja cubo 
        gl.glPushMatrix();
        gl.glColor3f(0,1,0);
        gl.glTranslated(-7,5,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutWireCube(1);
        gl.glPopMatrix();
        //dibuja cubo solido
        gl.glPushMatrix();
        gl.glColor3f(0,1,0);
        gl.glTranslated(-4,5,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
         //dibuja cono
        gl.glPushMatrix();
        gl.glColor3f(0.439216f,0.858824f,0.576471f);
        gl.glTranslated(-7,3,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutWireCone(1,1,50,5);
        gl.glPopMatrix();
        //dibuja cono solido
        gl.glPushMatrix();
        gl.glColor3f(0.439216f,0.858824f,0.576471f);
        gl.glTranslated(-4,3,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutSolidCone(1,1,50,5);
        gl.glPopMatrix();
        //dibuja esfera
        gl.glPushMatrix();
        gl.glColor3f(0.62352f,0.372549f,0.623529f);
        gl.glTranslated(-7,0,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutWireSphere(1,10,10);
        gl.glPopMatrix();
        //dibuja esfera solido
        gl.glPushMatrix();
        gl.glColor3f(0.62352f,0.372549f,0.623529f);
        gl.glTranslated(-4,0,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutSolidSphere(1,10,10);
        gl.glPopMatrix();
        //dibuja torus
        gl.glPushMatrix();
        gl.glColor3f(1,0,0);
        gl.glTranslated(-7,-3,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutWireTorus(0.4,0.8,10,20);
        gl.glPopMatrix();
        //dibuja torus solido
        gl.glPushMatrix();
        gl.glColor3f(1,0,0);
        gl.glTranslated(-4,-3,-20);
        gl.glRotatef(20,10,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutSolidTorus(0.4,0.8,10,20);
        gl.glPopMatrix();
        //dibuja tetera
        gl.glPushMatrix();
        gl.glColor3f(0,1,0);
        gl.glTranslated(-7,-6,-20);
        gl.glRotatef(20,15,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutWireTeapot(1);
        gl.glPopMatrix();
        //dibuja tetera solida
        gl.glPushMatrix();
        gl.glColor3f(0,1,0);
        gl.glTranslated(-3.5,-6,-20);
        gl.glRotatef(20,15,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutSolidTeapot(1);
        gl.glPopMatrix();
         //dibuja tetraedro
        gl.glPushMatrix();
        gl.glColor3f(0,0,1);
        gl.glTranslated(0,5,-20);
        gl.glRotatef(20,15,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutWireTetrahedron();
        gl.glPopMatrix();
        //dibuja tetraedro solida
        gl.glPushMatrix();
        gl.glColor3f(0,0,1);
        gl.glTranslated(3,5,-20);
        gl.glRotatef(20,15,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
         //dibuja octaedro
        gl.glPushMatrix();
        gl.glColor3f(1,0,1);
        gl.glTranslated(0,3,-20);
        gl.glRotatef(50,20,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutWireOctahedron();
        gl.glPopMatrix();
        //dibuja octaedro solida
        gl.glPushMatrix();
        gl.glColor3f(1,0,1);
        gl.glTranslated(3,3,-20);
        gl.glRotatef(50,20,1,0);
        gl.glRotatef (ang,1,0,0);
        glut.glutSolidOctahedron();
        gl.glPopMatrix();
        //dibuja icosaedro
        gl.glPushMatrix();
        gl.glColor3f(0,1,1);
        gl.glTranslated(0,0,-20);
        gl.glRotatef(50,20,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutWireIcosahedron();
        gl.glPopMatrix();
        //dibuja icosaedro solida
        gl.glPushMatrix();
        gl.glColor3f(0,1,1);
        gl.glTranslated(3,0,-20);
        gl.glRotatef(50,20,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutSolidIcosahedron();
        gl.glPopMatrix();
        //dibuja dodecaedro
        gl.glPushMatrix();
        gl.glColor3f(1,1,0);
        gl.glTranslated(0,-3,-20);
        gl.glRotatef(60,45,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutWireDodecahedron();
        gl.glPopMatrix();
        //dibuja dodecaedro solida
        gl.glPushMatrix();
        gl.glColor3f(1,1,0);
        gl.glTranslated(4,-3,-20);
        gl.glRotatef(60,45,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutSolidDodecahedron();
        gl.glPopMatrix();
        //dibuja dodecaedro
        gl.glPushMatrix();
        gl.glColor3f(0,0.5f,1);
        gl.glTranslated(0,-6,-20);
        gl.glRotatef(60,45,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutWireRhombicDodecahedron();
        gl.glPopMatrix();
        //dibuja dodecaedro solida
        gl.glPushMatrix();
        gl.glColor3f(0,0.5f,1);
        gl.glTranslated(4,-6,-20);
        gl.glRotatef(60,45,1,0);
        gl.glRotatef (ang,1,ang,0);
        glut.glutSolidRhombicDodecahedron();
        gl.glPopMatrix();
        //dibuja cilindro
        gl.glPushMatrix();
        gl.glColor3f(1,0.498039f, 0);
        gl.glTranslatef(6, 5, -20);
        gl.glRotatef (45,0,1,0);
        gl.glRotatef (ang,1,0,0);
        glu.gluCylinder(quad, 0.5, 0.5, 1, 40, 40);
        gl.glPopMatrix();
        
        
       gl.glFlush();
       ang=ang+0.09f;
       
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

